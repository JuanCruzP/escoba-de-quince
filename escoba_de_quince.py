import random as rd
import itertools


def subListas(lista):
    resultado = []

    for k in range(1, len(lista) + 1):

        for listita in itertools.combinations(lista, k):
            resultado.append(list(listita))

    return resultado


def sumatoria(lista):               # hace lo mismo que sum
    suma = 0
    for i in range(len(lista)):
        suma += lista[i]
    return suma

# *detalle importante: se usan los valores del número de las cartas salvo en el caso de las figuras a las que se les restan 2
def valReal(num):
    if num >= 10:
        num -= 2
    return num

#print(sumatoria([2,3]))

def sacar_carta(lista_cambiar, carta_sacar):
    i = 0
    sin_sacar_aun = True
    while sin_sacar_aun:
        if lista_cambiar[i] == carta_sacar and sin_sacar_aun: # solo si coincide con el primer numero de la combinacion posible...
            sin_sacar_aun = False
            lista_cambiar.pop(i) # elimina del mazo de jugador la primer carta correspondiente al primer numero en juego_a_usar
        i += 1
    return lista_cambiar

### Funciones ###
# 1. Se mezcla el mazo
def generarMazo():
    mazo = list(range(1,cartas_por_palo+1))*palos # 4 palos distintos de 12 cartas cada uno
    rd.shuffle(mazo)    # mezcla numeros al azar
    # 8 (numero de 9s y 8s en mi mazo) los quiero sacar
    for i in range(palos):
        mazo = sacar_carta(mazo, 8)
        mazo = sacar_carta(mazo, 9)

    return mazo


# 2. Se reparten las cartas a cada jugador
def repartir (mazo, n):
    juego = []          # Lista que contine cartas de cada jugador

    for i in range(n):  # Segun el numero de jugadores...
        jugador = []

        while len(jugador) < repartidas: # Mientras que no tenga aun las 3 cartas..
            carta = mazo.pop() # Sacar del mazo
            jugador.append(carta) # Dar al jugador
        juego.append(jugador) # Agregar jugador con cartas al juego

    return juego


# 3. Se colocan 4 cartas en la mesa
def iniciarMesa(mazo, mesa):

    if len(mazo) >= repartidas_mesa:     # Si en el mazo quedan almenos 4 cartas
        while len(mesa) < 4: 
        #for i in range(repartidas_mesa): # reparto 4 cartas a la mesa
            carta = mazo.pop(0)  # saco cartas del mazo
            # print(carta)
            mesa.append(carta) # agrego a la mesa

    return mesa


# 4. Cada jugador tiene por objetivo elegir una las cartas que tiene en la mano (inicialmente tres en cada ronda)
def juegosPosibles(jug, mesa): # debe generar la lista de todos los juegos posibles que puede realizar un jugador
    combinacion_jugador = []            # solo guardare aca las combinaciones posibles que den 15 (usando 1 carta del jugador y cantidad indefinida del mazo)
    juegos_posibles = subListas(mesa)   # veo todas las combinaciones posibles a realizar con lo que tengo en la mesa
    #print(juegos_posibles)

    for i in range(len(juegos_posibles)): # Para cada combinacion de la mesa... (en base a esas combinaciones, voy a ver con que cartas del jugador da 15)
        #print(juegos_posibles[i])

        for j in range(len(jug)):# Y para cada carta que tengo...
            juego_posible_valor_real = []

            for k in range(len(juegos_posibles[i])): # verifico que el valor real sea n-2 para numeros mayores a 10
                carta_revalorizada = valReal(juegos_posibles[i][k])
                juego_posible_valor_real.append(carta_revalorizada)

            combinacion = sumatoria(juego_posible_valor_real) + valReal(jug[j])  # Sumar cada una de ellas...
            #print(sumatoria(juegos_posibles[i]), '+', jug[j], '=', combinacion)
            if combinacion == 15:                       # Solo si resulta en 15...
                jugada_correcta = [jug[j]] + juegos_posibles[i] # la primer carta(index=0) pertenece al jugador, el resto a la mesa
                combinacion_jugador.append(jugada_correcta)# Meto la combinacion de todas las cartas, las del jugador en una lista para diferenciarlas

    #print(combinacion_jugador)
    return combinacion_jugador


# 5. Es posible que un jugador tenga m´as de una opci´on para hacer un juego, en ese caso utilizar´a alguna estrategia que apunte a mejorar sus chances de obtener alguno de los puntos enumerados arriba.
def elegirMejor(juegos):
    vacio = [] 
    if juegos != vacio:
        condicion_juego = True
        juego_elegido = juegos[0]

        for i in range(len(juegos)): # Para cada juego
            len_juego_actual = len(juegos[i]) # Veo la cantidad de cartas usada

            for j in range(len(juegos)): # Realizo una comparacion
                len_juego_comparable = len(juegos[j]) # Viendo la cantidad de cartas del siguiente

                if len_juego_actual < len_juego_comparable: # Comparo con cada elemento de la lista en base a si toma mas cartas para combinar
                    juego_elegido = juegos[j]

        # print('Juego de mas cartas:', juego_elegido)

    else:
        condicion_juego = False
        juego_elegido = []
        #print('No hay Juego')

    return condicion_juego, juego_elegido


def jugar(mesa, jug, basa):
    # Recibe: cartas que estan en la mesa, cartas que tiene un jugador en ese momento (que debe ser no vacia, es decir, aun tiene que tener cartas por jugar), la lista de juegos realizados por el jugador hasta el momento (llamaremos a estas carta, la basa del jugador).
    # debe usar la funci´on juegosPosibles para obtener la lista de juegos posibles, luego usar esta lista como argumento de elegirMejor (siempre y cuando hubiera al menos un juego posible para hacer) con lo que sabr´a cu´al es la mejor opci´on.
    posibilidades = juegosPosibles(jug, mesa)
    #print('Jugadas posibles de Jugador:', posibilidades)

    condicion_juego, juego_a_usar = elegirMejor(posibilidades)
    print(f'Combinacion:{juego_a_usar} \n########')

    if condicion_juego == True: # Solo si hay juego posibles

        for i in range(len(juego_a_usar)): # Solo ingreso las cartas, no el conjunto. la basa me permitira ver cuantos numeros tendre dentro al final
            basa.append(juego_a_usar[i])

        jug = sacar_carta(jug, juego_a_usar.pop(0))

        for j in range(len(juego_a_usar)): # elimina de la mesa el resto de las cartas en juego_a_usar
            mesa = sacar_carta(mesa, juego_a_usar.pop(0))

    elif condicion_juego == False and jug != []:   # deja una carta en la mesa (pueden elegir la primera que tenga el jugador en la mano o usar alg´un criterio m´as astuto).
        mesa.append(jug.pop(0))
    
    print(f'Jugador Ahora:{jug} \nMesa Ahora: {mesa} \nBasa: {basa}')
    return jug, mesa, basa


def jugarRonda(mesa, jugs, basas):
    # recibe la lista de cartas que estan en la mesa
    conteo_jugador = 0
    n_jugadores_sin_cartas = 0 # utiliza la funcion jugar con cada uno de los jugadores hasta completar la ronda
    turno = 0

    while n_jugadores_sin_cartas/len(jugs) != 1:
        print(f'\n-----------------Turno {turno}-----------------')
        print(f'Jugadores: {jugs} \nBasas:{basas}')
        print(f'Jugadores sin cartas en mano: {n_jugadores_sin_cartas}/{len(jugs)}')

        print(f'\nJugador {conteo_jugador}: {jugs[conteo_jugador]} \nMesa: {mesa}')
        jugs[conteo_jugador], mesa, basas[conteo_jugador] = jugar(mesa, jugs[conteo_jugador], basas[conteo_jugador])  # primer elemento de la lista jugs corresponde con la mano del primer jugador cuya basa esta en el primer elemento de la lista basas.

        # Verificacion de ronda 
        # cada uno de los jugadores haya jugado sus tres cartas. Se espera que jugs y basas sean listas de listas conteniendos las manos y las basas de cada uno de los jugadores en orden, es decir el
        n_jugadores_sin_cartas = 0
        for j in range(len(jugs)):
            if len(jugs[j]) == 0:
                n_jugadores_sin_cartas += 1

        # Ciclo de turnos
        conteo_jugador += 1
        if conteo_jugador == len(jugs):
            conteo_jugador = 0

        turno += 1

    print('\n------------Fin de Juego------------')
    print(f'Jugadores: {jugs} \nBasas:{basas}')
    print(f'Jugadores sin cartas en mano: {n_jugadores_sin_cartas}/{len(jugs)}')
    
    return basas, mesa


def sumaPuntos(basas):
    empate = False
    index = None
    #ultimo_ganador = None
    for i in range(len(basas)): # Para cada basa
        len_basa_actual = len(basas[i]) # Veo la cantidad de cartas usada

        for j in range(len(basas)): # Realizo una comparacion
            len_basa_comparable = len(basas[j]) # Viendo la cantidad de cartas del siguiente

            if len_basa_actual < len_basa_comparable: # Comparo con cada elemento de la lista en base a si toma mas cartas para combinar
                ultimo_ganador = basas[j]
                index = j
                empate = False
    
    ultimo_ganador[0][0] += 1
            #elif len_basa_actual == len_basa_comparable and basas[i] != basas[j] and ganador != basas[i]: # No pude darme cuenta que condicion me hacia falta para verificar 
            #    empate = True
                
    return index, empate, ultimo_ganador

### Debugging ###
#print(len(mazo))
#print(mazo)

#Juego = True
#while Juego

### Variables Globales ###
cartas_por_palo = 12
palos = 4
repartidas = 3
repartidas_mesa = 4
jugadores = 3


def Main(n_jugadores):
    mazo_mezclado = generarMazo()
    print(mazo_mezclado)
    juegos_jugadores = repartir(mazo_mezclado, n_jugadores)

    mesa_inicial = []
    mesa_actual = iniciarMesa(mazo_mezclado, mesa_inicial)
    print('###-Jugadas-###')
  
    basa_jugadores = []
    for i in range(n_jugadores):
       basa_jugadores.append([]) # basa para cada jugador
       basa_jugadores[i].append([0]) # cuento cantidad de juegos

    ronda = 0
    while len(mazo_mezclado) > 0:
        juegos_jugadores = repartir(mazo_mezclado, n_jugadores)
        print(f'\n-#-#-#-#-#-#-#-#-Ronda {ronda}-#-#-#-#-#-#-#-#-')
        basa_jugadores, mesa_actual = jugarRonda(mesa_actual, juegos_jugadores, basa_jugadores)
        index, empate, ultimo_ganador = sumaPuntos(basa_jugadores)
        ronda += 1

        if empate == False:
          print(f'Gano Jugador {index}:{ultimo_ganador}')
        else: 
          print('Empate')

    print(f'Se termino el mazo: {mazo_mezclado}')
    print(f'Rondas {ronda}\nBasas: {basa_jugadores}')
    for i in range(len(basa_jugadores)):
      punto_actual = basa_jugadores[i][0]

      for j in range(len(basa_jugadores)):
          punto_comparable = basa_jugadores[j][0]

          if punto_actual < punto_comparable:
              ultimo_ganador =  basa_jugadores[j][0]
              indice = j
              
    print(f'Gano jugador {indice}')

Main(jugadores)